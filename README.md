# README #

This repository is for general purpose code example used in the Compass team.


## Jupyter notebooks in Git ##
It is recommended to use [nbstripout](https://github.com/kynan/nbstripout) to clear output cells from notebook prior to commit.